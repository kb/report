# -*- encoding: utf-8 -*-
import os

from decimal import Decimal

from django.utils import timezone
from reportlab import platypus
from reportlab.lib.enums import TA_RIGHT, TA_CENTER

# from reportlab.lib.pagesizes import A4, landscape
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import mm
from reportlab.pdfgen import canvas


class PDFReport:
    LEFT = 0
    CENTRE = 1
    RIGHT = 2

    def __init__(self):
        # Use the sample style sheet.
        style_sheet = getSampleStyleSheet()
        self.body = style_sheet["BodyText"]
        self.body_centre = ParagraphStyle(
            name="BodyTextRight", parent=self.body, alignment=TA_CENTER
        )
        self.body_right = ParagraphStyle(
            name="BodyTextRight", parent=self.body, alignment=TA_RIGHT
        )
        self.head_1 = style_sheet["Heading1"]
        self.head_1_centre = ParagraphStyle(
            name="Heading1Centre", parent=self.head_1, alignment=TA_CENTER
        )
        self.head_1_right = ParagraphStyle(
            name="Heading1Right", parent=self.head_1, alignment=TA_RIGHT
        )
        self.head_2 = style_sheet["Heading2"]
        self.head_2_centre = ParagraphStyle(
            name="Heading2Centre", parent=self.head_2, alignment=TA_CENTER
        )
        self.head_2_right = ParagraphStyle(
            name="Heading2Right", parent=self.head_2, alignment=TA_RIGHT
        )
        self.head_3 = style_sheet["Heading3"]
        self.head_3.fontName = "Helvetica-Bold"
        self.head_3_centre = ParagraphStyle(
            name="Heading3Centre", parent=self.head_3, alignment=TA_CENTER
        )
        self.head_3_right = ParagraphStyle(
            name="Heading3Right", parent=self.head_3, alignment=TA_RIGHT
        )
        self.head_4 = style_sheet["Heading4"]
        self.head_4.fontName = "Helvetica-Bold"
        self.head_4_centre = ParagraphStyle(
            name="Heading4Centre", parent=self.head_4, alignment=TA_CENTER
        )
        self.head_4_right = ParagraphStyle(
            name="Heading4Right", parent=self.head_4, alignment=TA_RIGHT
        )
        self.GRID_LINE_WIDTH = 0.2

    def _bold(self, text, align=LEFT):
        return self._para("<b>{}</b>".format(text), align)

    def _head_1(self, text, align=LEFT):
        if align == self.RIGHT:
            style = self.head_1_right
        elif align == self.CENTRE:
            style = self.head_1_centre
        else:
            style = self.head_1

        return platypus.Paragraph(text, style)

    def _head_2(self, text, align=LEFT):
        if align == self.RIGHT:
            style = self.head_2_right
        elif align == self.CENTRE:
            style = self.head_2_centre
        else:
            style = self.head_2
        return platypus.Paragraph(text, style)

    def _head_3(self, text, align=LEFT):
        if align == self.RIGHT:
            style = self.head_3_right
        elif align == self.CENTRE:
            style = self.head_3_centre
        else:
            style = self.head_3
        return platypus.Paragraph(text, style)

    def _head_4(self, text, align=LEFT):
        if align == self.RIGHT:
            style = self.head_4_right
        elif align == self.CENTRE:
            style = self.head_4_centre
        else:
            style = self.head_4
        return platypus.Paragraph(text, style)

    def _image(self, file_name):
        return platypus.Image(
            os.path.join(
                os.path.dirname(os.path.realpath(__file__)), "static", file_name
            )
        )

    def _para(self, text, align=None):
        """Put the text into a paragraph.

        .. tip:: This is a simple way to wrap the text.

        """
        if align == self.RIGHT:
            style = self.body_right
        elif align == self.CENTRE:
            style = self.body_centre
        else:
            style = self.body
        return platypus.Paragraph(text, style)

    def _round(self, value):
        return value.quantize(Decimal(".01"))


class NumberedCanvas(canvas.Canvas):
    """
    Copied from ActiveState, Improved ReportLab recipe for page x of y
    http://code.activestate.com/recipes/576832/
    """

    def __init__(self, *args, **kwargs):
        canvas.Canvas.__init__(self, *args, **kwargs)
        # canvas.Canvas.setPageSize( landscape(A4) )
        self._saved_page_states = []

    def showPage(self):
        self._saved_page_states.append(dict(self.__dict__))
        self._startPage()

    def save(self):
        """add page info to each page (page x of y)"""
        num_pages = len(self._saved_page_states)
        for state in self._saved_page_states:
            self.__dict__.update(state)
            self.draw_page_number(num_pages)
            canvas.Canvas.showPage(self)
        canvas.Canvas.save(self)

    def draw_page_number(self, page_count):
        self.setFont("Helvetica", 7)
        now = timezone.localtime(timezone.now()).strftime("%d/%m/%Y %H:%M")
        self.drawCentredString(
            200 * mm,
            20 * mm,
            f"Page {self._pageNumber} of {page_count} - Printed {now}",
        )
