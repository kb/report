# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings


logger = logging.getLogger(__name__)


@dramatiq.actor(
    queue_name=settings.DRAMATIQ_QUEUE_NAME,
    max_retries=0,
    time_limit=3600000,
)
def process_reports():
    """Process outstanding reports.

    The default time limit is 10 minutes.
    https://dramatiq.io/guide.html#message-time-limits

    I changed this to 1 hour (I hope)!
    We have reports taking 20 minutes to run...

    """
    from report.models import ReportSchedule

    logger.info("Running reports...")
    schedule_pks = ReportSchedule.objects.process()
    logger.info("Run reports (completed {})".format(len(schedule_pks)))
    return len(schedule_pks)
