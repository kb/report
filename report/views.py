# -*- encoding: utf-8 -*-
import csv
import logging
import os

from braces.views import (
    JSONResponseMixin,
    AjaxResponseMixin,
    LoginRequiredMixin,
)
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, ListView, View

from base.url_utils import url_with_querystring
from base.view_utils import BaseMixin, RedirectNextMixin, request_redirect_url
from report.forms import ReportSpecificationEmptyForm
from report.models import ReportError, ReportSchedule, ReportSpecification


logger = logging.getLogger(__name__)


def send_report(request, file_path, mime_type, attachment):
    if os.path.exists(file_path):
        with open(file_path, "rb") as fh:
            response = HttpResponse(fh.read(), content_type=mime_type)
            response["Content-Disposition"] = "{}; filename={}".format(
                "attachment" if attachment else "inline",
                os.path.basename(file_path),
            )
            return response
    else:
        raise Http404


def download_report(request, pk):
    if request.user.is_anonymous:
        raise PermissionDenied()
    schedule = get_object_or_404(ReportSchedule, pk=pk)
    if request.user != schedule.user:
        raise PermissionDenied("You are not authorised to download this report")
    if not schedule.completed_date:
        raise ReportError(
            "No report output to download '{}'".format(schedule.pk)
        )
    if not schedule.output_file:
        raise ReportError("No report file to download '{}'".format(schedule.pk))
    try:
        if (
            hasattr(settings, "SEND_REPORT_FROM_DJANGO")
            and settings.SEND_REPORT_FROM_DJANGO
        ):
            return send_report(
                request, schedule.output_file.path, "text/csv", attachment=True
            )
        else:
            from django_sendfile import sendfile

            return sendfile(request, schedule.output_file.path, attachment=True)
    except ImportError:
        return send_report(
            request, schedule.output_file.path, "text/csv", attachment=True
        )


class ReportCsvMixin:
    """Display a CSV file.

    Typical usage::

      class DocumentIssueWipByTaskOwnerView(
          LoginRequiredMixin, ReportCsvMixin, BaseMixin, DetailView):

    If the url does not provide pk or slug you'll need to provide a
    get_object method (N.B. if the object does not exist e.g. first time
    the report is run you should put the report as a property of the class
    e.g::

      def get_object(self):
          report = ReportSpecification.objects.get(
              slug='issue-wip-task-owner'
          )
          return ReportSchedule.objects.completed(
              report, self.request.user
          ).last()

    To format the data e.g. to provide links you can override the
    ``format_data`` method

    The report app provides a helper template snippet which will format a
    table from the supplied titles and data.

    .. note:: I think this mixin is only used by ``DetailView``, so I don't
              think ``paginate_by`` is being used.
              I am using it now to limit the number of rows displayed in the
              table preview.

    """

    model = ReportSchedule
    paginate_by = 100
    is_preview = True

    def _check_file(self, output_file_path):
        result = False
        if os.path.exists(output_file_path):
            if os.path.isfile(output_file_path):
                result = True
            else:
                logger.error("'{}' is not a file".format(output_file_path))
        else:
            logger.error("'{}' does not exist".format(output_file_path))
        return result

    def _read_data(self, output_file_path):
        data = []
        titles = []
        count = 0
        csv.register_dialect(
            "default", skipinitialspace=True, quoting=csv.QUOTE_ALL
        )
        with open(output_file_path, encoding="utf-8") as csv_file:
            reader = csv.reader(csv_file, dialect="excel")
            for num, row in enumerate(reader):
                if num == 0:
                    titles = row[:]
                else:
                    count = count + 1
                    # show only the first 100 rows in the preview
                    if not self.is_preview or num <= self.paginate_by:
                        data.append(row[:])
        return titles, data, count

    def format_data(self, titles, data):
        """Format the report data.

        - override this method to format the data before rendering the
          template

        """
        return titles, data

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if not self.object:
            raise ReportError(
                "Report CSV mixin requires an object.  I am not sure when it "
                "wouldn't have one (as it is a detail view)"
            )
        # is the report scheduled
        scheduled = self.object.scheduled(self.request.user)
        data = []
        titles = []
        total_row_count = 0
        if self.object.output_file:
            if self._check_file(self.object.output_file.path):
                if self.object.report.is_csv():
                    titles, data, total_row_count = self._read_data(
                        self.object.output_file.path
                    )
        titles, data = self.format_data(titles, data)
        context.update(
            {
                "auto_refresh": self.request.GET.get("autoRefresh", None),
                "can_update_report": self.object.report.can_update_report,
                "data": data,
                "display_paginate_by_message": len(data) >= self.paginate_by,
                "paginate_by": self.paginate_by,
                "report": self.object.report,
                "report_titles": titles,
                "scheduled": scheduled,
                "title": self.object.title,
                "total_row_count": total_row_count,
            }
        )
        return context


class ReportSpecificationScheduleMixin:
    """Create a new schedule for this report specification."""

    form_class = ReportSpecificationEmptyForm
    model = ReportSpecification

    def form_valid(self, form):
        with transaction.atomic():
            self.schedule = self.object.schedule(self.request.user)
        return HttpResponseRedirect(self.get_success_url())

    def get_success_url(self):
        return reverse("report.schedule.csv.view", args=[self.schedule.pk])

    def test_func(self, user):
        specification = self.get_object()
        report_class = specification.import_class()
        return report_class().user_passes_test(user)


class ReportSpecificationView(LoginRequiredMixin, DetailView):
    model = ReportSpecification

    def get(self, request, *args, **kwargs):
        pk = int(kwargs["specification_pk"])
        key = kwargs.get("key")
        value = kwargs.get("value")
        next_url = request.GET.get("next")
        parameters = None
        if key and value:
            parameters = {key: value}
        report = ReportSpecification.objects.get(pk=pk)
        now = timezone.now()
        schedule = report.schedule(request.user, parameters)
        if schedule.created < now:
            messages.info(request, "{} Report is already queued".format(report))
        return HttpResponseRedirect(next_url)


class ScheduleListView(
    LoginRequiredMixin, RedirectNextMixin, BaseMixin, ListView
):
    paginate_by = 20

    def get_queryset(self):
        return ReportSchedule.objects.current(self.request.user).order_by(
            "-created"
        )


class ScheduleCompleteAPIView(JSONResponseMixin, AjaxResponseMixin, View):
    def get_ajax(self, request, pk, *args, **kwargs):
        authorised = False
        completed = False
        if request.user.is_authenticated:
            try:
                schedule = ReportSchedule.objects.get(pk=pk)
                completed = schedule.completed_date is not None
                authorised = schedule.user == request.user
            except ReportSchedule.DoesNotExist:
                pass
        if not authorised:
            raise PermissionDenied("You are not authorised to view this report")
        return self.render_json_response({"report_completed": completed})


class ScheduleCsvView(
    LoginRequiredMixin, RedirectNextMixin, ReportCsvMixin, BaseMixin, DetailView
):
    """A user can only view their own reports."""

    template_name = "report/report_csv.html"

    def get_object(self):
        schedule = super().get_object()
        if schedule.user == self.request.user:
            pass
        else:
            raise PermissionDenied
        return schedule


class ScheduleCsvLatestView(
    LoginRequiredMixin, RedirectNextMixin, ReportCsvMixin, BaseMixin, DetailView
):
    template_name = "report/report_csv.html"

    def get_object(self):
        specification_pk = self.kwargs.get("specification_pk")
        self.report = ReportSpecification.objects.get(pk=specification_pk)
        result = self.report.latest(user=self.request.user)
        if not result:
            result = ReportSchedule(report=self.report)
        return result


class ReportFormViewMixin(RedirectNextMixin):
    """**Usage**

    Either set a `report_class` and `form_class` on a CBV which handles its
    own url::

      from app.forms import AppForm
      from app.reports import AppReport

      class SingleReport(AuthEtc, ReportFormViewMixin, BaseMixin, FormView):
          report_class = AppReport
          form_class = AppForm  #<- Optional if `AppReport` has `form_class`.

    ... if the report does not require a form::

      class SingleReport(
              AuthEtc, ReportFormViewMixin, BaseMixin, TemplateView):
          report_class = AppReport

    .. note:: This uses a ``TemplateView`` rather than the ``FormView``.

    .. warning:: PK 09/10/2020, Not sure if this works, because a
                ``TemplateView`` will never call ``form_valid``.
                It may be easier to create an empty form and use this for the
                ``form_class``?

    Or set ``report_classes`` on a CBV, and ensure each Report listed has its
    own ``form_class``::

      from app.reports import AppReport1, AppReport2, AppReport3

      class SingleReport(AuthEtc, ReportFormViewMixin, BaseMixin, FormView):
          report_classes = [AppReport1, AppReport2, AppReport3]

    ... if no reports require a form::

      class SingleReport(AuthEtc, ReportFormViewMixin, BaseMixin, TemplateView):
          report_classes = [AppReport1, AppReport2, AppReport3]

    .. note:: This uses a ``TemplateView`` rather than the ``FormView``.

    """

    template_name = "report/report_form.html"

    def get_report_class(self):
        if hasattr(self, "report_class"):
            return self.report_class
        if not hasattr(self, "report_classes"):
            raise ReportError(
                "'{}' is missing `report_class' or "
                "'report_classes' attribute".format(self.report_class.__name__)
            )
        slug = self.kwargs.get("slug")
        if not slug:
            raise ReportError(
                "'{}' URL is missing 'slug' parameter "
                "matching one of its 'report_classes'".format(
                    self.report_class.__name__
                )
            )
        report_lookup = {
            report_class.REPORT_SLUG: report_class
            for report_class in self.report_classes
        }
        report_cls = report_lookup.get(slug)
        if not report_cls:
            raise ReportError(
                "The 'slug' parameter ('{}') did not match 'REPORT_SLUG' of "
                "any of its 'report_classes'.  For more information, see "
                "'report.views.ReportFormViewMixin'".format(slug)
            )
        return report_cls

    def get_form_class(self):
        if hasattr(self, "form_class"):
            if self.form_class:
                return self.form_class
        report_cls = self.get_report_class()
        if not hasattr(report_cls, "form_class"):
            raise ReportError(
                "'{}' is missing a 'form_class' attribute".format(
                    report_cls.__name__
                )
            )
        return report_cls.form_class

    def get_process_task(self):
        """Option to set the background task function."""
        try:
            return self.process_task
        except AttributeError:
            return None

    def _report_specification(self):
        report_cls = self.get_report_class()
        try:
            return ReportSpecification.objects.get(slug=report_cls.REPORT_SLUG)
        except ReportSpecification.DoesNotExist:
            return report_cls().init_report()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        report_cls = self.get_report_class()
        context.update(dict(report_title=report_cls.REPORT_TITLE))
        return context

    def form_valid(self, form):
        report_specification = self._report_specification()
        with transaction.atomic():
            report_schedule = report_specification.schedule(
                self.request.user,
                parameters=form.cleaned_data,
                title="{} for {:%d %B %Y %H:%M:%S}".format(
                    report_specification.title, timezone.now()
                ),
                process_task=self.get_process_task(),
            )
        kwargs = {}
        next_url = request_redirect_url(self.request)
        if next_url:
            kwargs.update(next=next_url)
        return HttpResponseRedirect(
            url_with_querystring(
                reverse("report.schedule.csv.view", args=[report_schedule.pk]),
                **kwargs,
            )
        )


class ReportsMenuViewMixin:
    """**Usage**

    Set a `reports_menu` as a simple list: [<reports>]::

        from app.reports import AppReport1, AppReport2, AppReport3

        class ReportsMenuViewMixin(AuthEtc, ReportMenuViewMixin, BaseMixin, TemplateView):
            reports_menu = [AppReport1, AppReport2, AppReport3]

    Or add Grouping levels listing tuple pairs: [("<group_heading>", [<reports>]), ...]::

        from app.reports import UserReport1, UserReport2, SalesReport1, SalesReport2

        class ReportsMenuViewMixin(AuthEtc, ReportMenuViewMixin, BaseMixin, TemplateView):
            reports_menu = [
                ("Customers", [AppReport1, AppReport2]),
                ("Sales", [SalesReport1, SalesReport2]),
            ]
    """

    template_name = "report/reports_menu.html"

    def _report_list(self, reports):
        return [
            {"title": r.REPORT_TITLE, "slug": r.REPORT_SLUG} for r in reports
        ]

    def _reports_menu_from_tuple(self, data):
        """Convert the reports menu into a format suitable for display.

        The ``data`` will be in this format::

          [
              (
                  "Enquiries",
                  [EnquiryMonthReport],
              ),
              (
                  "Another Section",
                  [AnotherReport, YetAnotherReport],
              ),
          ]

        """
        result = []
        for title, report_classes in data:
            row = (title, self._report_list(report_classes))
            result.append(row)
        return result

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        result = None
        if not hasattr(self, "reports_menu"):
            raise ReportError(
                "'ReportsMenuViewMixin' missing 'reports_menu' attribute"
            )
        if callable(self.reports_menu):
            result = self._reports_menu_from_tuple(self.reports_menu())
        elif isinstance(self.reports_menu, list):
            if len(self.reports_menu):
                if isinstance(self.reports_menu[0], tuple):
                    result = self._reports_menu_from_tuple(self.reports_menu)
                else:
                    result = [("Reports", self._report_list(self.reports_menu))]
        if not result:
            raise ReportError(
                "'ReportsMenuViewMixin' - 'reports_menu' is an invalid format"
            )
        context.update(dict(reports_menu=result))
        return context
