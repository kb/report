# -*- encoding: utf-8 -*-
from django import forms

from report.models import ReportSpecification


class ReportParametersEmptyForm(forms.Form):
    """Default form to create a report (no parameters).

    For more information, see ``form_class`` in:
    https://www.kbsoftware.co.uk/docs/app-report.html#new-report

    """

    pass


class ReportParametersFromToDateForm(forms.Form):
    from_date = forms.DateField(
        label="From",
        widget=forms.NumberInput(attrs={"type": "date"}),
    )
    to_date = forms.DateField(
        label="To",
        widget=forms.NumberInput(attrs={"type": "date"}),
    )


class ReportSpecificationEmptyForm(forms.ModelForm):
    class Meta:
        model = ReportSpecification
        fields = ()
