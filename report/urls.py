# -*- encoding: utf-8 -*-
from django.urls import re_path

from .views import (
    download_report,
    ReportSpecificationView,
    ScheduleCompleteAPIView,
    ScheduleCsvLatestView,
    ScheduleCsvView,
    ScheduleListView,
)


urlpatterns = [
    re_path(
        r"^schedule/(?P<pk>\d+)/complete/$",
        view=ScheduleCompleteAPIView.as_view(),
        name="report.schedule.api.complete",
    ),
    re_path(
        r"^schedule/(?P<pk>\d+)/download/$",
        view=download_report,
        name="report.schedule.download",
    ),
    re_path(
        r"^schedule/(?P<specification_pk>\d+)/latest/$",
        view=ScheduleCsvLatestView.as_view(),
        name="report.schedule.csv.latest",
    ),
    re_path(
        r"^schedule/(?P<pk>\d+)/view/$",
        view=ScheduleCsvView.as_view(),
        name="report.schedule.csv.view",
    ),
    re_path(
        r"^schedule/$",
        view=ScheduleListView.as_view(),
        name="report.schedule.list",
    ),
    re_path(
        r"^(?P<specification_pk>\d+)/$",
        view=ReportSpecificationView.as_view(),
        name="report.specification",
    ),
]
