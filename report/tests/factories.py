# -*- encoding: utf-8 -*-
import factory

from login.tests.factories import UserFactory
from report.models import ReportSpecification, ReportSchedule


class ReportSpecificationFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = ReportSpecification

    app = "example_report"
    report_class = "SampleReport"

    @factory.sequence
    def slug(n):
        return "slug_{:02d}".format(n)

    @factory.sequence
    def title(n):
        return "title_{:02d}".format(n)


class ReportScheduleFactory(factory.django.DjangoModelFactory):
    report = factory.SubFactory(ReportSpecificationFactory)
    user = factory.SubFactory(UserFactory)
    max_retry_count = ReportSchedule.DEFAULT_MAX_RETRY_COUNT
    parameters = {}

    class Meta:
        model = ReportSchedule
