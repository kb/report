# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command

from report.models import ReportSchedule
from report.tests.factories import ReportScheduleFactory


@pytest.mark.django_db
def test_process():
    """Test the management command"""
    ReportScheduleFactory()
    # ReportSchedule.objects.process()
    call_command("run_reports")
