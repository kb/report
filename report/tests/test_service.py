# -*- encoding: utf-8 -*-
import pytest

from datetime import datetime

from report.models import ReportError
from report.service import date_or_none, int_or_none, top


@pytest.mark.parametrize(
    "value,expect",
    [
        ("2021-01-31", datetime(2021, 1, 31)),
    ],
)
def test_date_or_none(value, expect):
    parameters = {"extra": value}
    assert expect == date_or_none(parameters, "extra")


def test_int_or_none():
    parameters = {"contact": 23}
    assert 23 == int_or_none(parameters, "contact", "ExampleReport")


def test_int_or_none_empty_string():
    parameters = {"contact": ""}
    assert int_or_none(parameters, "contact", "ExampleReport") is None


def test_int_or_none_invalid():
    parameters = {"contact": "A1B2"}
    with pytest.raises(ReportError) as e:
        int_or_none(parameters, "contact", "ExampleReport")
    assert "'ExampleReport' cannot get 'contact', 'A1B2'" in str(e.value)


def test_int_or_none_spaces():
    parameters = {"contact": "  "}
    assert int_or_none(parameters, "contact", "ExampleReport") is None


def test_int_or_none_string():
    parameters = {"contact": "23"}
    assert 23 == int_or_none(parameters, "contact", "ExampleReport")


def test_int_or_none_with_none():
    parameters = {"contact": None}
    assert int_or_none(parameters, "contact", "ExampleReport") is None


@pytest.mark.django_db
def test_top():
    data = {
        "a": 100,
        "b": 101,
        "c": 102,
        "d": 103,
        "e": 104,
        "f": 105,
        "g": 106,
        "h": 80,
        "i": 70,
        "j": 60,
    }
    result = top(data)
    assert {
        "a": 100,
        "b": 101,
        "c": 102,
        "d": 103,
        "e": 104,
        "f": 105,
        "g": 106,
        "h": 80,
        "other": 130,
    } == result


@pytest.mark.django_db
def test_top_one():
    data = {"a": 100}
    result = top(data)
    assert {"a": 100} == result
