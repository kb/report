# -*- encoding: utf-8 -*-
from report.views import ReportsMenuViewMixin

from braces.views import (
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    UserPassesTestMixin,
)
from django.urls import reverse
from django.views.generic import (
    CreateView,
    DetailView,
    FormView,
    TemplateView,
    UpdateView,
)

from base.view_utils import BaseMixin
from enquiry.views import EnquiryCreateMixin
from report.models import ReportSchedule, ReportSpecification
from report.views import (
    ReportCsvMixin,
    ReportFormViewMixin,
    ReportSpecificationScheduleMixin,
)
from .forms import EmptyExampleForm
from .reports import (
    SampleReport,
    SampleReportPdf,
    SampleReportWithQueueAndProcessTask,
)
from .tasks import process_reports_example


class EnquiryCreateView(EnquiryCreateMixin, BaseMixin, CreateView):
    """Save an enquiry in the database."""

    def get_success_url(self):
        return reverse("project.home")


class HomeView(BaseMixin, TemplateView):
    template_name = "example/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.user.is_authenticated:
            report_list = ReportSchedule.objects.completed(
                report=None, user=self.request.user
            )
        else:
            report_list = ReportSchedule.objects.none()
        context.update({"report_list": report_list})
        return context


class SettingsView(TemplateView):
    template_name = "example/settings.html"


class PrintReport(LoginRequiredMixin, ReportCsvMixin, BaseMixin, DetailView):
    template_name = "example/print_report.html"
    is_preview = False


class ProduceReport(LoginRequiredMixin, ReportCsvMixin, BaseMixin, DetailView):
    template_name = "report/report_csv.html"

    def get_object(self):
        """Get the current report (or create a new one)."""
        slug = self.kwargs.get("slug")
        report = ReportSpecification.objects.get(slug=slug)
        result = report.current_schedule(self.request.user)
        if not result:
            result = report.schedule(self.request.user)
        return result


class ReportSpecificationScheduleView(
    LoginRequiredMixin,
    ReportSpecificationScheduleMixin,
    UserPassesTestMixin,
    BaseMixin,
    UpdateView,
):
    """Create a new schedule for this report specification."""

    pass


class ExampleReportFormViewNoTask(
    LoginRequiredMixin,
    ReportFormViewMixin,
    BaseMixin,
    FormView,
):
    form_class = EmptyExampleForm
    process_task = process_reports_example
    report_classes = [
        SampleReport,
        SampleReportWithQueueAndProcessTask,
    ]


class ExampleReportFormViewWithTask(
    LoginRequiredMixin,
    ReportFormViewMixin,
    BaseMixin,
    FormView,
):
    form_class = EmptyExampleForm
    report_classes = [
        SampleReport,
        SampleReportWithQueueAndProcessTask,
    ]


class ExampleReportsMenuInvalidFormatView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ReportsMenuViewMixin,
    BaseMixin,
    TemplateView,
):
    reports_menu = {"aaa": "bbb"}


class ExampleReportsMenuListView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ReportsMenuViewMixin,
    BaseMixin,
    TemplateView,
):
    reports_menu = [SampleReport, SampleReportWithQueueAndProcessTask]


class ExampleReportsMenuMethodView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ReportsMenuViewMixin,
    BaseMixin,
    TemplateView,
):
    def reports_menu(self):
        return [
            (
                "Sample",
                [
                    SampleReport,
                ],
            ),
            (
                "Another Section",
                [
                    SampleReportPdf,
                    SampleReportWithQueueAndProcessTask,
                ],
            ),
        ]


class ExampleReportsMenuMissingAttributeView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ReportsMenuViewMixin,
    BaseMixin,
    TemplateView,
):
    pass


class ExampleReportsMenuView(
    LoginRequiredMixin,
    StaffuserRequiredMixin,
    ReportsMenuViewMixin,
    BaseMixin,
    TemplateView,
):
    reports_menu = [
        (
            "Sample",
            [
                SampleReport,
                SampleReportPdf,
            ],
        ),
        (
            "Another Section",
            [SampleReportWithQueueAndProcessTask],
        ),
    ]
