#!/bin/bash
# exit immediately if a command exits with a nonzero exit status.
set -e
# treat unset variables as an error when substituting.
set -u

echo Drop database: $DATABASE_NAME
if [[ -z "${DATABASE_HOST}" ]]; then
  psql -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
  psql -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
else
  PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "DROP DATABASE IF EXISTS $DATABASE_NAME;"
  PGPASSWORD=$DATABASE_PASS psql --host $DATABASE_HOST --port $DATABASE_PORT -U $DATABASE_USER -c "CREATE DATABASE $DATABASE_NAME TEMPLATE=template0 ENCODING='utf-8';"
fi

python manage.py migrate --noinput
python manage.py demo_data_login
python manage.py init_app_enquiry
python manage.py demo_report
python manage.py runserver 0.0.0.0:8000
