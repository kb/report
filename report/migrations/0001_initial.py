# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [migrations.swappable_dependency(settings.AUTH_USER_MODEL)]

    operations = [
        migrations.CreateModel(
            name="Report",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("slug", models.SlugField()),
                ("caption", models.CharField(max_length=100)),
                ("chart_type", models.CharField(max_length=100)),
                (
                    "user",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        to=settings.AUTH_USER_MODEL,
                        on_delete=models.CASCADE,
                    ),
                ),
            ],
            options={
                "ordering": ("slug",),
                "verbose_name": "Report",
                "verbose_name_plural": "Reports",
            },
        ),
        migrations.CreateModel(
            name="ReportDataInteger",
            fields=[
                (
                    "id",
                    models.AutoField(
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                        auto_created=True,
                    ),
                ),
                ("created", models.DateTimeField(auto_now_add=True)),
                ("modified", models.DateTimeField(auto_now=True)),
                ("x", models.CharField(max_length=50)),
                ("y", models.IntegerField()),
                (
                    "report",
                    models.ForeignKey(
                        to="report.Report", on_delete=models.CASCADE
                    ),
                ),
            ],
            options={
                "ordering": ("report", "y"),
                "verbose_name": "Report data",
                "verbose_name_plural": "Report data",
            },
        ),
        migrations.AlterUniqueTogether(
            name="report", unique_together=set([("slug", "user")])
        ),
    ]
