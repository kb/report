# -*- encoding: utf-8 -*-
import os
import pytest

from django.core.files import File
from django.test import override_settings
from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from login.tests.factories import UserFactory, TEST_PASSWORD
from report.models import ReportError, ReportSchedule
from report.tests.factories import (
    ReportScheduleFactory,
    ReportSpecificationFactory,
)


def _create_output_file(report_schedule):
    """Create a CSV file for a report schedule.

    Code copied from:
    http://www.revsys.com/blog/2014/dec/03/loading-django-files-from-code/

    """
    full_path = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "data", "test.csv"
    )
    with open(full_path, "r") as f:
        django_file = File(f)
        report_schedule.output_file.save(
            report_schedule.output_file_name, django_file, save=True
        )


@pytest.mark.django_db
def test_report_csv_view(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    schedule = ReportScheduleFactory(user=u)
    _create_output_file(schedule)
    # test
    url = reverse("report.schedule.csv.view", args=[schedule.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_report_csv_view_invalid_user(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    schedule = ReportScheduleFactory(user=UserFactory())
    _create_output_file(schedule)
    # test
    url = reverse("report.schedule.csv.view", args=[schedule.pk])
    response = client.get(url)
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_report_csv_view_no_output_file(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    schedule = ReportScheduleFactory(user=u, output_file=None)
    url = reverse("report.schedule.csv.view", args=[schedule.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_schedule_list(client):
    user = UserFactory()
    report = ReportSpecificationFactory()
    ReportScheduleFactory(report=report, title="a", user=user)
    ReportScheduleFactory(report=report, title="b", user=UserFactory())
    ReportScheduleFactory(report=report, title="c", user=user)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    # test
    url = reverse("report.schedule.list")
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert "object_list" in response.context
    assert "reportschedule_list" in response.context
    qs = response.context["reportschedule_list"]
    assert ["c", "a"] == [x.title for x in qs]


@pytest.mark.django_db
def test_download_report_does_not_exist(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    schedule = ReportScheduleFactory(user=u, completed_date=timezone.now())
    url = reverse("report.schedule.download", args=[schedule.pk])
    with pytest.raises(ReportError) as e:
        client.get(url)
    assert "No report file to download" in str(e.value)


@override_settings(SEND_REPORT_FROM_DJANGO=True)
@pytest.mark.django_db
def test_download_report_django_send(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    schedule = ReportScheduleFactory(user=u, completed_date=timezone.now())
    _create_output_file(schedule)
    url = reverse("report.schedule.download", args=[schedule.pk])
    response = client.get(url)
    # this will need to be updated when Malcolm removes ``sendfile`` on Windows
    assert 200 == response.status_code


@override_settings(SEND_REPORT_FROM_DJANGO=True)
@pytest.mark.django_db
def test_download_report_django_send_does_not_exist(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    schedule = ReportScheduleFactory(user=u, completed_date=timezone.now())
    _create_output_file(schedule)
    os.remove(schedule.output_file.path)
    url = reverse("report.schedule.download", args=[schedule.pk])
    response = client.get(url)
    # this will need to be updated when Malcolm removes ``sendfile`` on Windows
    assert HTTPStatus.NOT_FOUND == response.status_code


@pytest.mark.django_db
def test_download_report_anonymous(client):
    """User must be logged in to download."""
    schedule = ReportScheduleFactory()
    url = reverse("report.schedule.download", args=[schedule.pk])
    response = client.get(url)
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_download_report_another_user(client):
    """User can only download their own reports."""
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    schedule = ReportScheduleFactory(user=UserFactory())
    url = reverse("report.schedule.download", args=[schedule.pk])
    response = client.get(url)
    assert HTTPStatus.FORBIDDEN == response.status_code


@pytest.mark.django_db
def test_schedule_create_view(client):
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    report = ReportSpecificationFactory(
        slug="test",
        title="Test",
        app="example_report",
        report_class="SampleReport",
    )
    response = client.post(
        reverse("project.report.specification.schedule", args=[report.pk])
    )
    assert HTTPStatus.FOUND == response.status_code
    schedule = ReportSchedule.objects.get(report=report)
    assert user == schedule.user
    assert "" == schedule.title
    assert (
        reverse("report.schedule.csv.view", args=[schedule.pk]) == response.url
    )


@pytest.mark.django_db
def test_schedule_create_view_slug(client):
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    report = ReportSpecificationFactory(
        slug="test",
        title="Test",
        app="example_report",
        report_class="SampleReport",
    )
    response = client.post(
        reverse("project.report.specification.schedule", args=[report.slug])
    )
    assert HTTPStatus.FOUND == response.status_code
    schedule = ReportSchedule.objects.get(report=report)
    assert user == schedule.user
    assert "" == schedule.title
    assert (
        reverse("report.schedule.csv.view", args=[schedule.pk]) == response.url
    )


@pytest.mark.django_db
def test_view_latest(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    report = ReportSpecificationFactory()
    # 1
    ReportScheduleFactory(report=report, user=u, completed_date=timezone.now())
    # 2
    schedule = ReportScheduleFactory(
        report=report, user=u, completed_date=timezone.now()
    )
    _create_output_file(schedule)
    # test
    url = reverse("report.schedule.csv.latest", args=[report.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    assert schedule == response.context["object"]
    assert response.context["can_update_report"] is True


@pytest.mark.django_db
def test_view_latest_can_update(client):
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    report = ReportSpecificationFactory(can_update_report=False)
    ReportSchedule.objects.create_report_schedule(report, user=user)
    # test
    response = client.get(
        reverse("report.schedule.csv.latest", args=[report.pk])
    )
    assert HTTPStatus.OK == response.status_code
    assert response.context["can_update_report"] is False


@pytest.mark.django_db
def test_view_latest_no_report_schedule(client):
    u = UserFactory()
    assert client.login(username=u.username, password=TEST_PASSWORD) is True
    report_specification = ReportSpecificationFactory(can_update_report=False)
    # test
    url = reverse("report.schedule.csv.latest", args=[report_specification.pk])
    response = client.get(url)
    assert HTTPStatus.OK == response.status_code
    schedule = response.context["object"]
    assert schedule.completed_date is None
    assert "No report available" in str(response.content)
