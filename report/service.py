# -*- encoding: utf-8 -*-
from dateutil import parser
from decimal import Decimal

from .models import ReportError, ReportSpecification


def date_or_none(parameters, variable_name):
    """Use in a report to get an 'datetime' value from a dictionary.

    .. tip:: Code copied from ``api/api_utils.py``).

    ::

      from datetime import datetime, time
      from report.service import date_or_none

      def get_queryset(self):
          from_date = date_or_none(parameters, "from")
          if from_date:
              # set the time to the start of the day::
              from_date = datetime.combine(from_date.date(), time.min)
          to_date = date_or_none(self.request, "to")
          if to_date:
              # set the time to the end of the day::
              to_date = datetime.combine(to_date.date(), time.max)

    """
    result = None
    date_as_str = parameters.get(variable_name, None)
    if date_as_str:
        result = parser.parse(date_as_str)
    return result


def int_or_none(parameters, variable_name, class_name):
    """Use in a report to get an integer value from a dictionary.

    .. tip:: The ``class_name`` is only used to throw a helpful exception
             message.

    .. tip:: Code copied from ``api/api_utils.py``).

    ::

      from report.service import int_or_none

      def run_csv_report(self, csv_writer, parameters=None):
          if parameters:
          contact = int_or_none(
              parameters, "contact", self.__class__.__name__
          )

    """
    result = parameters.get(variable_name) or None
    try:
        result = result.strip() or None
    except AttributeError:
        pass
    if result:
        try:
            result = int(result)
        except ValueError:
            raise ReportError(
                "'{}' cannot get '{}', '{}'".format(
                    class_name, variable_name, result
                )
            )
    return result


def top(data):
    """Merge the last 10 percent into an 'other' value."""
    total = 0
    for value in data.values():
        total = total + value
    trigger = int(Decimal(total) * Decimal(".9"))
    result = {}
    count = 0
    other = 0
    total = 0
    for k in sorted(data, key=data.get, reverse=True):
        count = count + 1
        value = data[k]
        total = total + value
        # preserve at least 6 items
        if count < 6:
            result[k] = value
        # if in the last 10 percent then merge
        elif total < trigger:
            result[k] = value
        else:
            other = other + value
    if other:
        result["other"] = other
    return result


class ReportMixin:
    def init_report(self):
        module_name, _ = self.__module__.split(".")
        help_text = None
        output_format = None
        try:
            help_text = self.REPORT_HELP
        except AttributeError:
            pass
        try:
            output_format = self.REPORT_FORMAT
        except AttributeError:
            pass
        return ReportSpecification.objects.init_report_specification(
            slug=self.REPORT_SLUG,
            title=self.REPORT_TITLE,
            app=module_name,
            report_class=self.__class__.__name__,
            can_update_report=False,
            help_text=help_text,
            output_format=output_format,
        )
