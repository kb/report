# -*- encoding: utf-8 -*-
import pytest

from example_report.reports import SampleReport, SampleReportPdf
from report.models import ReportSpecification


@pytest.mark.django_db
def test_init_report():
    assert 0 == ReportSpecification.objects.count()
    report = SampleReport()
    x = report.init_report()
    assert 1 == ReportSpecification.objects.count()
    specification = ReportSpecification.objects.first()
    assert x == specification
    assert "SampleReport" == specification.report_class
    assert "example_report" == specification.app
    assert "csv" == specification.output_format
    assert "My Sample Report" == specification.title
    assert specification.can_update_report is False


@pytest.mark.django_db
def test_init_report_pdf():
    assert 0 == ReportSpecification.objects.count()
    report = SampleReportPdf()
    x = report.init_report()
    assert 1 == ReportSpecification.objects.count()
    specification = ReportSpecification.objects.first()
    assert x == specification
    assert "SampleReportPdf" == specification.report_class
    assert "example_report" == specification.app
    assert "pdf" == specification.output_format
    assert "My Sample PDF Report" == specification.title
    assert specification.can_update_report is False
