# -*- encoding: utf-8 -*-
"""
More tests in ``example_report/tests/test_report_schedule.py``
"""
import pytest

from dateutil.relativedelta import relativedelta
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils import timezone

from login.tests.factories import UserFactory
from report.tests.factories import (
    ReportSpecificationFactory,
    ReportScheduleFactory,
)
from report.models import ReportError, ReportSchedule


@pytest.mark.django_db
def test_output_file_name():
    user = UserFactory(username="staff")
    report = ReportSpecificationFactory(slug="sample")
    schedule = ReportScheduleFactory(report=report, user=user)
    assert (
        "sample-staff-{}.csv".format(schedule.created.strftime("%Y%m%d-%H%M%S"))
        == schedule.output_file_name
    )


@pytest.mark.django_db
def test_output_file_name_no_user():
    report = ReportSpecificationFactory(slug="sample")
    schedule = ReportScheduleFactory(report=report, user=None)
    assert (
        "sample-system-{}.csv".format(
            schedule.created.strftime("%Y%m%d-%H%M%S")
        )
        == schedule.output_file_name
    )


@pytest.mark.django_db
def test_report_schedule():
    user = UserFactory(username="staff")
    report = ReportSpecificationFactory(slug="sample")
    schedule = ReportScheduleFactory(report=report, user=user)
    expect_name = "sample-staff-{}.csv".format(
        schedule.created.strftime("%Y%m%d-%H%M%S")
    )
    assert schedule.output_file_name == expect_name


@pytest.mark.django_db
def test_create_report_schedule():
    staff = UserFactory(username="staff")
    spec = ReportSpecificationFactory(
        slug="squares",
        title="Squares and Cubes",
        app="example_report",
        report_class="SampleReport",
    )
    now = timezone.now()
    parameters = {"process_key": "timeOff"}
    assert 0 == ReportSchedule.objects.count()
    schedule = ReportSchedule.objects.create_report_schedule(
        report=spec, user=staff, parameters=parameters
    )
    assert 1 == ReportSchedule.objects.count()
    assert parameters == schedule.parameters
    assert schedule.created >= now
    # run again to make check that a new schedule is created
    now = timezone.now()
    parameters = {"process_key": "invoiceApproval"}
    schedule = ReportSchedule.objects.create_report_schedule(
        report=spec, user=staff, parameters=parameters, title="Apple"
    )
    assert 2 == ReportSchedule.objects.count()
    assert "" == schedule.queue_name
    assert schedule.created >= now
    assert parameters == schedule.parameters
    assert staff == schedule.user
    assert "Apple" == schedule.title


@pytest.mark.django_db
def test_create_report_schedule_queue_name():
    report_schedule = ReportSchedule.objects.create_report_schedule(
        ReportSpecificationFactory(
            app="example_report",
            report_class="SampleReportWithQueueAndProcessTask",
        ),
        user=UserFactory(),
        parameters={},
    )
    # This is 'settings.DRAMATIQ_QUEUE_NAME'
    assert "dev_test_report_queue_name" == report_schedule.queue_name


@pytest.mark.django_db
def test_current():
    r1 = ReportScheduleFactory()
    r2 = ReportScheduleFactory()
    # r2.set_deleted(UserFactory())
    r3 = ReportScheduleFactory()
    assert [r1.pk, r2.pk, r3.pk] == [
        x.pk for x in ReportSchedule.objects.current()
    ]


@pytest.mark.django_db
def test_current_user():
    user = UserFactory()
    r1 = ReportScheduleFactory(user=user)
    r2 = ReportScheduleFactory()
    # r2.set_deleted(UserFactory())
    ReportScheduleFactory()
    r4 = ReportScheduleFactory(user=user)
    assert [r1.pk, r4.pk] == [
        x.pk for x in ReportSchedule.objects.current(user)
    ]


@pytest.mark.django_db
def test_scheduled():
    user = UserFactory()
    report = ReportSpecificationFactory()
    r1 = ReportScheduleFactory(report=report, user=user)
    ReportScheduleFactory(
        report=report, user=user, completed_date=timezone.now()
    )
    # default report has a maximum retry count of 10
    ReportScheduleFactory(report=report, user=user, retries=11)
    r4 = ReportScheduleFactory(report=report, user=user)
    result = []
    for report_schedule in ReportSchedule.objects.all():
        if report_schedule.scheduled(user):
            result.append(report_schedule.pk)
    assert set([r1.pk, r4.pk]) == set(result)


@pytest.mark.django_db
def test_scheduled_system():
    """System reports have no user."""
    user = UserFactory()
    report = ReportSpecificationFactory()
    r1 = ReportScheduleFactory(report=report, user=None)
    ReportScheduleFactory(
        report=report, user=None, completed_date=timezone.now()
    )
    # default report has a maximum retry count of 10
    ReportScheduleFactory(report=report, user=None, retries=11)
    r4 = ReportScheduleFactory(report=report, user=None)
    ReportScheduleFactory(report=report, user=user)
    result = []
    for report_schedule in ReportSchedule.objects.all():
        if report_schedule.scheduled():
            result.append(report_schedule.pk)
    assert set([r1.pk, r4.pk]) == set(result)


@pytest.mark.django_db
def test_completed():
    ReportScheduleFactory()
    r2 = ReportScheduleFactory(user=None, completed_date=timezone.now())
    # 3
    r3 = ReportScheduleFactory(user=None, completed_date=timezone.now())
    # r3.set_deleted(UserFactory())
    # 4
    ReportScheduleFactory(user=None)
    # 5
    r5 = ReportScheduleFactory(user=None, completed_date=timezone.now())
    assert [r2.pk, r3.pk, r5.pk] == [
        x.pk for x in ReportSchedule.objects.completed()
    ]


@pytest.mark.django_db
def test_completed_report():
    report = ReportSpecificationFactory()
    r1 = ReportScheduleFactory(
        report=report, user=None, completed_date=timezone.now()
    )
    ReportScheduleFactory(user=None, completed_date=timezone.now())
    r3 = ReportScheduleFactory(
        report=report, user=None, completed_date=timezone.now()
    )
    ReportScheduleFactory(
        report=report, user=UserFactory(), completed_date=timezone.now()
    )
    assert [r1.pk, r3.pk] == [
        x.pk for x in ReportSchedule.objects.completed(report=report)
    ]


@pytest.mark.django_db
def test_completed_report_and_user():
    report = ReportSpecificationFactory()
    user = UserFactory()
    r1 = ReportScheduleFactory(
        report=report, user=user, completed_date=timezone.now()
    )
    ReportScheduleFactory(report=report, completed_date=timezone.now())
    r3 = ReportScheduleFactory(
        report=report, user=user, completed_date=timezone.now()
    )
    ReportScheduleFactory(user=user, completed_date=timezone.now())
    qs = ReportSchedule.objects.completed(report=report, user=user)
    assert [r1.pk, r3.pk] == [x.pk for x in qs]


@pytest.mark.django_db
def test_completed_user():
    user = UserFactory()
    r1 = ReportScheduleFactory(user=user, completed_date=timezone.now())
    ReportScheduleFactory(completed_date=timezone.now())
    r3 = ReportScheduleFactory(user=user, completed_date=timezone.now())
    assert [r1.pk, r3.pk] == [
        x.pk for x in ReportSchedule.objects.completed(user=user)
    ]


# @pytest.mark.django_db
# def test_completed_with_output_file():
#    """Test 'completed_with_output_file'."""
#    t1 = timezone.now() + relativedelta(days=-3)
#    t2 = timezone.now() + relativedelta(days=-2)
#    t3 = timezone.now() + relativedelta(days=-1)
#    t4 = timezone.now()
#    r1 = ReportSpecificationFactory(title="Bb")
#    user = UserFactory()
#    ReportScheduleFactory(
#        report=r1,
#        title="a",
#        completed_date=t1,
#        user=user,
#        output_file=SimpleUploadedFile("my.csv", b"file contents"),
#    )
#    ReportScheduleFactory(
#        report=r1,
#        title="b",
#        completed_date=t1,
#        user=user,
#        output_file=SimpleUploadedFile("my.csv", b"file contents"),
#    ).delete()
#    ReportScheduleFactory(
#        report=r1,
#        title="c",
#        user=user,
#        output_file=SimpleUploadedFile("my.csv", b"file contents"),
#    )
#    ReportScheduleFactory(
#        report=r1,
#        title="d",
#        user=user,
#        completed_date=t2,
#        output_file=None,
#    )
#    ReportScheduleFactory(
#        report=r1,
#        title="e",
#        user=user,
#        completed_date=t3,
#        output_file=SimpleUploadedFile("my.csv", b"file contents"),
#    )
#    r2 = ReportSpecificationFactory(title="Aa")
#    ReportScheduleFactory(
#        report=r2,
#        title="x",
#        user=user,
#        completed_date=t4,
#        output_file=SimpleUploadedFile("my.csv", b"file contents"),
#    )
#    assert ["x", "a", "d", "e"] == [
#        x.title for x in ReportSchedule.objects.completed(user=user)
#    ]
#    assert ["x", "a", "e"] == [
#        x.title
#        for x in ReportSchedule.objects.completed_with_output_file(user=user)
#    ]


# @pytest.mark.django_db
# def test_output_file_exists():
#    report_schedule = ReportScheduleFactory(
#        title="x",
#        output_file=SimpleUploadedFile("my.csv", b"file contents"),
#    )
#    assert report_schedule.output_file_exists() is True


# @pytest.mark.django_db
# def test_output_file_exists_not():
#    report_schedule = ReportScheduleFactory(title="x", output_file=None)
#    assert report_schedule.output_file_exists() is False


@pytest.mark.django_db
def test_process():
    ReportScheduleFactory()
    ReportSchedule.objects.process()


@pytest.mark.django_db
def test_process_invalid_module(caplog):
    report_specification = ReportSpecificationFactory(
        app="does_not_exist", report_class="DoesNotExistReport"
    )
    ReportScheduleFactory(report=report_specification)
    assert [] == ReportSchedule.objects.process()
    found = False
    for x in caplog.records:
        if x.levelname == "ERROR":
            assert (
                "Cannot find 'DoesNotExistReport'. Do you have "
                "'reports.py' in your 'does_not_exist' package"
            ) in x.exc_text
            found = True
            break
    assert found is True


@pytest.mark.django_db
def test_outstanding():
    r1 = ReportScheduleFactory()
    ReportScheduleFactory(completed_date=timezone.now())
    # default report has a maximum retry count of 10
    ReportScheduleFactory(retries=11)
    r4 = ReportScheduleFactory()
    assert [r1.pk, r4.pk] == [
        x.pk for x in ReportSchedule.objects.outstanding()
    ]
