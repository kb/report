# -*- encoding: utf-8 -*-
"""
More tests in ``report/tests/test_report_schedule.py``
"""
import pytest

from example_report.tasks import process_reports_example
from login.tests.factories import UserFactory
from report.models import ReportSchedule
from report.tests.factories import (
    ReportSpecificationFactory,
    ReportScheduleFactory,
)


@pytest.mark.django_db
@pytest.mark.parametrize(
    "report_class,process_task",
    [
        ("SampleReport", None),
        ("SampleReportWithQueueAndProcessTask", process_reports_example),
    ],
)
def test_create_report_schedule_process_task(report_class, process_task):
    """Run this report on a specific background task queue?"""
    report_schedule = ReportSchedule.objects.create_report_schedule(
        ReportSpecificationFactory(
            app="example_report", report_class=report_class
        ),
        user=UserFactory(),
        parameters={},
    )
    assert process_task == report_schedule.process_task
