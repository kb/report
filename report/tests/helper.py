# -*- encoding: utf-8 -*-
import csv

from report.models import ReportSchedule, ReportSpecification


def run_test_report(user, report_slug, parameters=None):
    """Run a report for testing purposes.

    Keyword arguments:
    user -- The user running the report.
    report_slug -- The report slug e.g. ``EnquiryMonthReport.REPORT_SLUG``.
    parameters -- Report parameters e.g.

    Returns
    first_row -- First row of the report e.g. ``["Ticket", "Contact"]``.
    data -- report data e.g. ``[["Apple", "Patrick"]]``

    """
    # get the report, prepare the parameters and schedule the report
    report_specification = ReportSpecification.objects.get(slug=report_slug)
    report_specification.schedule(user, parameters=parameters)
    # run the report and find the schedule
    schedule_pks = ReportSchedule.objects.process()
    assert 1 == len(
        schedule_pks
    ), "Created {} reports (did your report raise an exception?)".format(
        len(schedule_pks)
    )
    schedule_pk = schedule_pks[0]
    schedule = ReportSchedule.objects.get(pk=schedule_pk)
    # check the report output
    reader = csv.reader(open(schedule.output_file.path), "excel")
    first_row = None
    data = []
    for row in reader:
        if not first_row:
            first_row = row
        else:
            data.append(row)
    return first_row, data
