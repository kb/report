# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from example_report.reports import (
    SampleReport,
    SampleReportWithQueueAndProcessTask,
)
from report.models import ReportError, ReportSchedule


@pytest.mark.django_db
def test_example_project_report_slug(client):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(
        reverse("project.report.slug", args=["example-sample-report"])
    )
    assert HTTPStatus.OK == response.status_code


@pytest.mark.django_db
def test_reports_view_menu(client):
    """Test 'reports_menu' as a tuple.

    Testing ``report.views.ReportsMenuViewMixin``

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("project.report"))
    assert HTTPStatus.OK == response.status_code
    assert "reports_menu" in response.context
    reports_menu = response.context["reports_menu"]
    assert [
        (
            "Sample",
            [
                {"title": "My Sample Report", "slug": "example-sample-report"},
                {
                    "title": "My Sample PDF Report",
                    "slug": "example-sample-pdf-report",
                },
            ],
        ),
        (
            "Another Section",
            [
                {
                    "title": "My Sample Report (Queue Name)",
                    "slug": "example-sample-report-queue",
                }
            ],
        ),
    ] == reports_menu


@pytest.mark.django_db
def test_reports_view_menu_invalid_format(client):
    """Test 'reports_menu' in an invalid format.

    Testing ``report.views.ReportsMenuViewMixin``

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(ReportError) as e:
        client.get(reverse("project.report.invalid.format"))
    assert (
        "'ReportsMenuViewMixin' - 'reports_menu' is an invalid format"
        in str(e.value)
    )


@pytest.mark.django_db
def test_reports_view_menu_list(client):
    """Test 'reports_menu' as a list.

    Testing ``report.views.ReportsMenuViewMixin``

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("project.report.list"))
    assert HTTPStatus.OK == response.status_code
    assert "reports_menu" in response.context
    reports_menu = response.context["reports_menu"]
    assert [
        (
            "Reports",
            [
                {"title": "My Sample Report", "slug": "example-sample-report"},
                {
                    "title": "My Sample Report (Queue Name)",
                    "slug": "example-sample-report-queue",
                },
            ],
        ),
    ] == reports_menu


@pytest.mark.django_db
def test_reports_view_menu_method(client):
    """Test 'reports_menu' as a method.

    Testing ``report.views.ReportsMenuViewMixin``

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    response = client.get(reverse("project.report.method"))
    assert HTTPStatus.OK == response.status_code
    assert "reports_menu" in response.context
    reports_menu = response.context["reports_menu"]
    assert [
        (
            "Sample",
            [{"title": "My Sample Report", "slug": "example-sample-report"}],
        ),
        (
            "Another Section",
            [
                {
                    "title": "My Sample PDF Report",
                    "slug": "example-sample-pdf-report",
                },
                {
                    "title": "My Sample Report (Queue Name)",
                    "slug": "example-sample-report-queue",
                },
            ],
        ),
    ] == reports_menu


@pytest.mark.django_db
def test_reports_view_menu_missing_attribute(client):
    """Test a missing 'reports_menu' attribute.

    Testing ``report.views.ReportsMenuViewMixin``

    """
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    with pytest.raises(ReportError) as e:
        client.get(reverse("project.report.missing.attribute"))
    assert "'ReportsMenuViewMixin' missing 'reports_menu' attribute" in str(
        e.value
    )


@pytest.mark.django_db
@pytest.mark.parametrize(
    "url_name,report_class",
    [
        ("example.report.with.task", SampleReport),
        ("example.report.with.task", SampleReportWithQueueAndProcessTask),
        ("project.report.slug", SampleReport),
        ("project.report.slug", SampleReportWithQueueAndProcessTask),
    ],
)
def test_example_report(client, url_name, report_class):
    user = UserFactory(is_staff=True)
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    assert 0 == ReportSchedule.objects.count()
    url = reverse(url_name, args=[report_class.REPORT_SLUG])
    response = client.post(url)
    assert HTTPStatus.FOUND == response.status_code, response.context[
        "form"
    ].errors
    assert 1 == ReportSchedule.objects.count()
    report_schedule = ReportSchedule.objects.first()
    assert (
        reverse("report.schedule.csv.view", args=[report_schedule.pk])
        == response.url
    )
