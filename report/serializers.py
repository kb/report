# -*- encoding: utf-8 -*-
from django.core.serializers.json import DjangoJSONEncoder
from django.urls import reverse
from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from .models import ReportSchedule, ReportSpecification


class ReportScheduleSerializer(serializers.ModelSerializer):
    slug = serializers.CharField(allow_null=True, required=False)
    retries = serializers.ReadOnlyField(required=False)
    download_url = serializers.SerializerMethodField()
    download_file_name = serializers.SerializerMethodField()

    class Meta:
        model = ReportSchedule
        fields = (
            "id",
            "created",
            "completed_date",
            "download_file_name",
            "download_url",
            "is_complete",
            "parameters",
            "retries",
            "slug",
        )

    def create(self, validated_data):
        user = self.context.get("user")
        if not user:
            raise ReportError("Cannot create a report schedule without a user")
        report_schedule = ReportSpecification.objects.schedule(
            validated_data.get("slug"),
            user=user,
            parameters=validated_data.get("parameters"),
        )
        return report_schedule

    def get_download_file_name(self, instance):
        return "{}-{}-{}.{}".format(
            instance.pk,
            instance.report.slug,
            instance.created.strftime("%Y-%m-%d-%H-%M"),
            instance.report.output_format,
        )

    def get_download_url(self, instance):
        return reverse("report.download", args=[instance.pk])

    def update(self, instance, validated_data):
        raise MethodNotAllowed(
            "GET", detail='Method "GET" not allowed without lookup'
        )
