# -*- encoding: utf-8 -*-
import os

from django.core.files.base import ContentFile, File
from django.http import (
    HttpResponse,
    HttpResponseForbidden,
    HttpResponseNotFound,
)
from rest_framework import views, viewsets
from rest_framework.authentication import TokenAuthentication
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.permissions import IsAuthenticated

from .models import ReportSchedule
from .serializers import ReportScheduleSerializer


class ReportDownloadView(views.APIView):
    """Download a report.

    Copied from ``work.api.WorkDownloadView``

    """

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def _download_report(self, report_schedule_pk, user):
        """Download report."""
        response = HttpResponseForbidden()
        try:
            report_schedule = ReportSchedule.objects.get(pk=report_schedule_pk)
            if user == report_schedule.user:
                with open(report_schedule.output_file.path, "rb") as fh:
                    response = HttpResponse(fh.read(), content_type="text/csv")
                    response["Content-Disposition"] = "{}; filename={}".format(
                        "attachment",
                        os.path.basename(report_schedule.output_file.path),
                    )
        except ReportSchedule.DoesNotExist:
            response = HttpResponseNotFound()
        return response

    def get(self, request, pk, format=None):
        """Download a report."""
        return self._download_report(pk, request.user)


class ReportScheduleViewSet(viewsets.ModelViewSet):
    """Create a report schedule and retrieve details."""

    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ReportScheduleSerializer

    def get_queryset(self):
        return ReportSchedule.objects.current()

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"user": self.request.user})
        return context

    def perform_destroy(self, instance):
        raise MethodNotAllowed(
            "GET", detail='Method "GET" not allowed without lookup'
        )
