# -*- encoding: utf-8 -*-
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, re_path
from django.urls import path, reverse_lazy
from django.views.generic import RedirectView
from rest_framework import routers

from report.api import ReportDownloadView, ReportScheduleViewSet
from .views import (
    EnquiryCreateView,
    ExampleReportFormViewNoTask,
    ExampleReportFormViewWithTask,
    ExampleReportsMenuInvalidFormatView,
    ExampleReportsMenuListView,
    ExampleReportsMenuMethodView,
    ExampleReportsMenuMissingAttributeView,
    ExampleReportsMenuView,
    HomeView,
    PrintReport,
    ProduceReport,
    ReportSpecificationScheduleView,
    SettingsView,
)

router = routers.DefaultRouter()
router.register(
    r"report-schedules",
    ReportScheduleViewSet,
    basename="report-schedule",
)

urlpatterns = [
    re_path(r"^api/0.1/", view=include((router.urls, "api"), namespace="api")),
    re_path(r"^$", view=HomeView.as_view(), name="project.home"),
    re_path(r"^$", view=SettingsView.as_view(), name="project.settings"),
    re_path(r"^", view=include("login.urls")),
    re_path(
        r"^add/$",
        view=EnquiryCreateView.as_view(),
        name="example.enquiry.create",
    ),
    re_path(r"^enquiry/", view=include("enquiry.urls")),
    re_path(
        r"^example/report/(?P<slug>[-\w\d]+)/no/task/$",
        view=ExampleReportFormViewNoTask.as_view(),
        name="project.report.slug",
    ),
    re_path(
        r"^example/report/(?P<slug>[-\w\d]+)/with/task/$",
        view=ExampleReportFormViewWithTask.as_view(),
        name="example.report.with.task",
    ),
    re_path(
        r"^home/user/$",
        view=RedirectView.as_view(url=reverse_lazy("project.home")),
        name="project.dash",
    ),
    re_path(r"^report/", view=include("report.urls")),
    re_path(
        r"^report/missing/invalid/format/$",
        view=ExampleReportsMenuInvalidFormatView.as_view(),
        name="project.report.invalid.format",
    ),
    re_path(
        r"^report/$",
        view=ExampleReportsMenuView.as_view(),
        name="project.report",
    ),
    re_path(
        r"^report/list/$",
        view=ExampleReportsMenuListView.as_view(),
        name="project.report.list",
    ),
    re_path(
        r"^report/method/$",
        view=ExampleReportsMenuMethodView.as_view(),
        name="project.report.method",
    ),
    re_path(
        r"^report/missing/attribute/$",
        view=ExampleReportsMenuMissingAttributeView.as_view(),
        name="project.report.missing.attribute",
    ),
    path(
        "report/<slug:slug>/",
        view=ProduceReport.as_view(),
        name="project.report.view",
    ),
    path(
        "report/<int:pk>/print/",
        view=PrintReport.as_view(),
        name="project.report.csv.print",
    ),
    re_path(
        r"^report/(?P<pk>\d+)/download/$",
        view=ReportDownloadView.as_view(),
        name="report.download",
    ),
    re_path(
        r"^report/(?P<pk>\d+)/schedule/$",
        view=ReportSpecificationScheduleView.as_view(),
        name="project.report.specification.schedule",
    ),
    re_path(
        r"^report/(?P<slug>[-\w\d]+)/schedule/$",
        view=ReportSpecificationScheduleView.as_view(),
        name="project.report.specification.schedule",
    ),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#   ^ helper function to return a URL pattern for serving files in debug mode.
# https://docs.djangoproject.com/en/1.5/howto/static-files/#serving-files-uploaded-by-a-user

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        re_path(r"^__debug__/", include(debug_toolbar.urls))
    ] + urlpatterns
