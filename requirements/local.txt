-r base.txt
-e .
-e ../api
-e ../base
-e ../enquiry
-e ../gdpr
-e ../login
-e ../mail
attrs
black
django-debug-toolbar
django_extensions
factory-boy
freezegun
GitPython
pytest-cov
pytest-django
pytest-flakes
pytest-pep8
PyYAML
rich
semantic-version
walkdir
