# -*- encoding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from report.models import ReportSchedule, ReportSpecification


class Command(BaseCommand):
    def handle(self, *args, **options):
        spec = ReportSpecification.objects.init_report_specification(
            slug="squares",
            title="Squares and Cubes",
            app="example_report",
            module="reports",
            report_class="SampleReport",
        )
        ReportSchedule.objects.create_report_schedule(
            report=spec, user=User.objects.get(username="staff")
        )
        self.stdout.write("Created 'report' demo")
