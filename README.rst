report
******

Django application for off-line reporting.

Install
=======

Virtual Environment
-------------------

::

  virtualenv --python=python3 venv-report
  source venv-report/bin/activate

  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  py.test -x

Release
=======

https://www.kbsoftware.co.uk/docs/
