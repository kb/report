# -*- encoding: utf-8 -*-
import csv
import importlib
import io
import logging

# import pathlib
import tempfile

from django.conf import settings
from django.core.files import File
from django.core.files.base import ContentFile
from django.core.serializers.json import DjangoJSONEncoder
from django.db import models
from django.db import transaction
from django.db.models import Q
from django.utils import timezone
from report.tasks import process_reports
from reversion import revisions as reversion

from base.model_utils import (
    private_file_store,
    RetryModel,
    RetryModelManager,
    TimedCreateModifyDeleteModel,
)


logger = logging.getLogger(__name__)


class ReportError(Exception):
    def __init__(self, value):
        Exception.__init__(self)
        self.value = value

    def __str__(self):
        return repr("%s, %s" % (self.__class__.__name__, self.value))


class ReportSpecificationManager(models.Manager):
    def init_report_specification(
        self,
        slug,
        title,
        app,
        report_class,
        module=None,
        can_update_report=None,
        help_text=None,
        output_format=None,
    ):
        updated = False
        if help_text is None:
            help_text = ""
        if module is None:
            module = self.model.DEFAULT_MODULE
        try:
            spec = ReportSpecification.objects.get(slug=slug)
        except ReportSpecification.DoesNotExist:
            spec = ReportSpecification(slug=slug)
            updated = True
        if spec.help_text != help_text:
            spec.help_text = help_text
            updated = True
        if spec.title != title:
            spec.title = title
            updated = True
        if can_update_report is None:
            can_update_report = True
        if output_format and spec.output_format != output_format:
            spec.output_format = output_format
            update = True
        if spec.can_update_report != can_update_report:
            spec.can_update_report = can_update_report
            updated = True
        if spec.app != app:
            spec.app = app
            updated = True
        if spec.report_class != report_class:
            spec.report_class = report_class
            updated = True
        if spec.module != module:
            spec.module = module
            updated = True
        if updated:
            spec.save()
        return spec

    def schedule(self, slug, user, parameters=None, title=None):
        try:
            specification = self.model.objects.get(slug=slug)
        except self.model.DoesNotExist:
            raise ReportError("Cannot find '{}' report".format(slug))
        return specification.schedule(user, parameters, title)


class ReportSpecification(TimedCreateModifyDeleteModel):
    """Specification of a report.

    Identifies a class that contains the report code.

    .. note:: ref ``can_update_report``, If a report has parameters, then it
              might not make sense for the user to be able to click the
              'Update Report' button.

    """

    FORMAT_CSV = "csv"
    FORMAT_JSON = "json"
    FORMAT_PDF = "pdf"

    DEFAULT_MAX_RETRIES = 10
    DEFAULT_MODULE = "reports"
    DEFAULT_FORMAT = FORMAT_CSV

    slug = models.SlugField(unique=True)
    title = models.CharField(max_length=200)
    help_text = models.TextField()
    app = models.CharField(max_length=100)
    module = models.CharField(max_length=100, default=DEFAULT_MODULE)
    report_class = models.CharField(max_length=100)
    required_parameters = models.JSONField(blank=True, null=True)
    output_format = models.CharField(max_length=100, default=DEFAULT_FORMAT)
    can_update_report = models.BooleanField(
        default=True, help_text="Can the user click the 'Update Report' button"
    )
    max_retries = models.PositiveIntegerField(default=DEFAULT_MAX_RETRIES)
    objects = ReportSpecificationManager()

    class Meta:
        ordering = ("slug",)
        verbose_name = "Report Specification"
        verbose_name_plural = "Report Specifications"

    def __str__(self):
        return "{}".format(self.title)

    def completed(self):
        return (
            self.reportschedule_set.filter(completed_date__isnull=False)
            # .exclude(deleted=True)
            .order_by("-completed_date")
        )

    def current_schedule(self, user):
        qs = self.reportschedule_set.filter(user=user)  # .exclude(deleted=True)
        return qs.order_by("-completed_date", "-created").first()

    def import_class(self):
        """Import the report module and get the class."""
        class_name = "{}.{}".format(self.app, self.module)
        try:
            report_module = importlib.import_module(class_name)
        except ImportError as e:
            # ``ModuleNotFoundError`` not available until python 3.6
            message = (
                "Cannot find '{}'. Do you have '{}.py' "
                "in your '{}' package? ({})".format(
                    self.report_class, self.module, self.app, str(e)
                )
            )
            raise ReportError(message)
        # get the class
        report_class = getattr(report_module, self.report_class)
        return report_class

    def is_csv(self):
        return self.output_format == self.FORMAT_CSV

    def latest(self, user=None):
        return ReportSchedule.objects.completed(self, user).last()

    def schedule(self, user, parameters=None, title=None, process_task=None):
        """Schedule this report specification.

        ``process_task`` is the function which adds the report to the
        background task queue.

        - The default is ``process_reports`` (``report/tasks.py``)
        - The developer can pass a ``process_task`` function to this method.
        - The report class can include a ``PROCESS_TASK`` attribute.

        """
        report_schedule = ReportSchedule.objects.create_report_schedule(
            self, user, parameters, title
        )
        if process_task is None:
            process_task = report_schedule.process_task
        if process_task is None:
            process_task = process_reports
        transaction.on_commit(lambda: process_task.send())
        logger.info(
            "report schedule: {} ({})".format(
                report_schedule.title or report_schedule.report.title,
                report_schedule.pk,
            )
        )
        return report_schedule

    def user_passes_test(self, user):
        report_class = self.import_class()
        return report_class().user_passes_test(user)


reversion.register(ReportSpecification)


class ReportScheduleManager(RetryModelManager):
    def create_report_schedule(
        self, report, user=None, parameters=None, title=None
    ):
        """Create a report schedule.

        Discussed with Malcolm.  We will create a new report schedule each time.

        """
        schedule = ReportSchedule(
            report=report,
            max_retry_count=self.model.DEFAULT_MAX_RETRY_COUNT,
        )
        report_class = report.import_class()
        try:
            schedule.queue_name = report_class.QUEUE_NAME
        except AttributeError:
            # Default - processed by 'process_reports' ('report/tasks.py')
            pass
        if user:
            schedule.user = user
        if parameters:
            schedule.parameters = parameters
        if title:
            schedule.title = title
        schedule.save()
        return schedule

    def current(self, user=None):
        qs = self.model.objects.all()
        if user:
            qs = qs.filter(user=user)
        return qs

    def completed(self, report=None, user=None):
        qs = super().completed()
        if user is None:
            qs = qs.filter(user__isnull=True)
        else:
            qs = qs.filter(user=user)
        if report:
            qs = qs.filter(report=report)
        return qs.order_by("report__title", "completed_date")

    # def completed_with_output_file(self, report=None, user=None):
    #    """Display completed reports which have an 'output_file'.
    #    If the report has no data, then the ``output_file`` may be empty.
    #    """
    #    return self.completed(report, user).exclude(
    #        Q(output_file="") | Q(output_file__exact=None)
    #    )


class ReportSchedule(RetryModel):
    """A scheduled run of a report.

    The ``title`` field will commonly be used where a report has parameters
    e.g. a from or to date.  As the date changes, the title will change.

    """

    DEFAULT_MAX_RETRY_COUNT = 5

    report = models.ForeignKey(ReportSpecification, on_delete=models.CASCADE)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        blank=True,
        null=True,
        on_delete=models.CASCADE,
    )
    title = models.CharField(max_length=200, blank=True)
    parameters = models.JSONField(
        blank=True, null=True, encoder=DjangoJSONEncoder
    )
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    completed_date = models.DateTimeField(blank=True, null=True)
    output_file = models.FileField(
        upload_to="report/output",
        storage=private_file_store,
        max_length=100,
        blank=True,
        null=True,
    )
    # output_file_path_and_file same as we use in record or is it the connector?
    # Or perhaps we shouldn't put a full path in here as it may change or need
    # updating?
    # Perhaps we just put in the domain name e.g.
    # nms.ark.navigatorterminals.com
    # from:
    # /home/web/repo/files/nms.ark.navigatorterminals.com/private/

    objects = ReportScheduleManager()

    class Meta:
        ordering = ("report", "user__username", "completed_date")
        verbose_name = "Scheduled Report"
        verbose_name_plural = "Scheduled Reports"

    def __str__(self):
        return "{} {}".format(self.report, self.user)

    # def get_download_url(self, instance):
    #    return reverse("report.download", args=[this.pk])

    # def output_file_exists(self):
    #    result = False
    #    if self.output_file:
    #        file_path = pathlib.Path(self.output_file.path)
    #        result = file_path.exists()
    #    return result

    @property
    def output_file_name(self):
        return "{}-{}-{}.{}".format(
            self.report.slug,
            self.user.username if self.user else "system",
            self.created.strftime("%Y%m%d-%H%M%S"),
            self.report.output_format,
        )

    def process(self):
        """

        Code to copy the temporary file into a Django field copied from:
        http://www.revsys.com/blog/2014/dec/03/loading-django-files-from-code/

        To stop the extra new-lines (blank lines) in Windows:
        https://stackoverflow.com/questions/3191528/csv-in-python-adding-an-extra-carriage-return

        .. tip:: The ``report_method`` may be named ``run_csv_report``.

        """
        result = False
        report = self.report
        report_class = report.import_class()
        # get the method - using standardised method name
        report_method = "run_{}_report".format(report.output_format)
        if hasattr(report_class, report_method):
            report_instance = report_class()
            report_instance.schedule = self
            run_method = getattr(report_class, report_method)
            if report.output_format == ReportSpecification.FORMAT_CSV:
                with tempfile.TemporaryFile(
                    mode="w+", newline="", encoding="utf-8"
                ) as csv_file:
                    output_stream = csv.writer(
                        csv_file, dialect="excel", lineterminator="\n"
                    )
                    try:
                        run_method(
                            report_instance,
                            output_stream,
                            parameters=self.parameters,
                        )
                        csv_file.seek(0)
                        self.completed_date = timezone.now()
                        self.output_file.save(
                            self.output_file_name, csv_file, save=True
                        )
                        # schedule_pks.append(schedule.pk)
                        logger.info(
                            "Report '{}' complete ({})".format(
                                self.title or self.report.title,
                                self.pk,
                            )
                        )
                        result = True
                    except ReportError as e:
                        logger.error(e)
                        logger.info(
                            "Retry count for '{}' ({}) is {}".format(
                                self.report.title,
                                self.pk,
                                self.retries,
                            )
                        )
            elif report.output_format == ReportSpecification.FORMAT_JSON:
                f = tempfile.NamedTemporaryFile(delete=False)
                run_method(
                    report_instance,
                    f.name,
                    parameters=self.parameters,
                )
                self.completed_date = timezone.now()

                reopen = open(f.name, "rb")
                django_file = File(reopen)
                self.output_file.save(
                    self.output_file_name, django_file, save=True
                )

                logger.info(
                    "Report '{}' complete ({})".format(
                        self.title or self.report.title,
                        self.pk,
                    )
                )
                result = True
            elif report.output_format == ReportSpecification.FORMAT_PDF:
                buff = io.BytesIO()
                run_method(
                    report_instance,
                    buff,
                    parameters=self.parameters,
                )
                pdf = buff.getvalue()
                buff.close()
                self.completed_date = timezone.now()
                self.output_file.save(
                    self.output_file_name, ContentFile(pdf), save=True
                )
                logger.info(
                    "Report '{}' complete ({})".format(
                        self.title or self.report.title,
                        self.pk,
                    )
                )
                result = True
            else:
                logger.error(
                    "Report '{}': Unknown format '{}'".format(
                        self.report.title, report.output_format
                    )
                )
        else:
            logger.error(
                "Report class '{}' has no '{}' method".format(
                    report_class, report_method
                )
            )
        return result

    @property
    def process_task(self):
        """Run this report on a specific background task queue?"""
        result = None
        report = self.report
        report_class = report.import_class()
        try:
            result = report_class.PROCESS_TASK
        except AttributeError:
            pass
        return result

    def scheduled(self, user=None):
        result = not self.completed_date
        if result:
            result = self.retries <= self.report.max_retries
        if result:
            if user:
                result = user == self.user
            else:
                result = self.user is None
        return result


reversion.register(ReportSchedule)
