# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand

from report.models import ReportSchedule


class Command(BaseCommand):
    def handle(self, *args, **options):
        ReportSchedule.objects.process()
