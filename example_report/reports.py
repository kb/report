# -*- encoding: utf-8 -*-
from django.conf import settings

from report.forms import ReportParametersEmptyForm
from report.models import ReportSpecification
from report.service import ReportMixin
from .tasks import process_reports_example


class SampleReport(ReportMixin):
    """A sample report"""

    REPORT_SLUG = "example-sample-report"
    REPORT_TITLE = "My Sample Report"
    form_class = ReportParametersEmptyForm

    def run_csv_report(self, csv_writer, parameters=None):
        csv_writer.writerow(("number", "square", "cube"))
        for num in range(1, 201):
            csv_writer.writerow((num, num * num, num * num * num))
        return True

    def user_passes_test(self, user):
        return True


class SampleReportPdf(ReportMixin):
    """A sample report"""

    REPORT_SLUG = "example-sample-pdf-report"
    REPORT_TITLE = "My Sample PDF Report"
    REPORT_FORMAT = ReportSpecification.FORMAT_PDF

    def run_csv_report(self, csv_writer, parameters=None):
        csv_writer.writerow(("number", "square", "cube"))
        for num in range(1, 201):
            csv_writer.writerow((num, num * num, num * num * num))
        return True

    def user_passes_test(self, user):
        return True


class SampleReportWithQueueAndProcessTask(ReportMixin):
    """A sample report which needs to be run with a specific task queue.

    - For ``QUEUE_NAME``, see ``outstanding`` in ``base.model_utils.RetryModel``
    - See ``process_task`` in ``reports.models.ReportSchedule``.

    """

    PROCESS_TASK = process_reports_example
    QUEUE_NAME = "dev_test_report_queue_name"
    REPORT_SLUG = "example-sample-report-queue"
    REPORT_TITLE = "My Sample Report (Queue Name)"

    def run_csv_report(self, csv_writer, parameters=None):
        csv_writer.writerow(("number", "square", "cube"))
        for num in range(1, 201):
            csv_writer.writerow((num, num * num, num * num * num))
        return True

    def user_passes_test(self, user):
        return True
