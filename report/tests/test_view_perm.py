# -*- encoding: utf-8 -*-
import pytest

from django.urls import reverse

from login.tests.factories import UserFactory
from login.tests.fixture import perm_check
from report.tests.factories import (
    ReportScheduleFactory,
    ReportSpecificationFactory,
)


@pytest.mark.django_db
def test_schedule_create_view(perm_check):
    report = ReportSpecificationFactory(
        slug="test",
        title="Test",
        app="example_report",
        report_class="SampleReport",
    )
    url = reverse("project.report.specification.schedule", args=[report.pk])
    perm_check.auth(url)


@pytest.mark.django_db
def test_schedule_csv_latest(perm_check):
    report = ReportSpecificationFactory()
    url = reverse("report.schedule.csv.latest", args=[report.pk])
    perm_check.auth(url)


@pytest.mark.django_db
def test_schedule_list(perm_check):
    report = ReportSpecificationFactory()
    ReportScheduleFactory(report=report)
    url = reverse("report.schedule.list")
    perm_check.auth(url)


@pytest.mark.django_db
def test_specification_view(perm_check):
    report = ReportSpecificationFactory(
        slug="test",
        title="Test",
        app="example_report",
        report_class="SampleReport",
    )
    url = reverse("report.specification", args=[report.pk])
    perm_check.auth(url, expect=302)
