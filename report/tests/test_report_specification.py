# -*- encoding: utf-8 -*-
import pytest

from django.utils import timezone

from login.tests.factories import UserFactory
from report.models import ReportSpecification
from report.tests.factories import (
    ReportScheduleFactory,
    ReportSpecificationFactory,
)


@pytest.mark.django_db
def test_completed():
    """Test 'completed'."""
    t1 = timezone.now()
    t2 = timezone.now()
    r1 = ReportSpecificationFactory()
    ReportScheduleFactory(report=r1, title="a", completed_date=t1, retries=1)
    ReportScheduleFactory(
        report=r1, title="b", completed_date=t1, retries=2
    ).delete()
    ReportScheduleFactory(report=r1, title="c", retries=3)
    ReportScheduleFactory(report=r1, title="d", completed_date=t2, retries=4)
    r2 = ReportSpecificationFactory()
    ReportScheduleFactory(report=r2, title="x", completed_date=t2, retries=5)
    assert ["d", "a"] == [x.title for x in r1.completed()]


@pytest.mark.django_db
def test_current_schedule():
    user = UserFactory()
    r1 = ReportSpecificationFactory()
    t1 = timezone.now()
    s1 = ReportScheduleFactory(user=user, report=r1)
    ReportScheduleFactory(user=user, report=r1).delete()
    ReportScheduleFactory(user=user, report=r1, completed_date=t1)
    s2 = ReportScheduleFactory(
        user=user, report=r1, completed_date=timezone.now()
    )
    ReportScheduleFactory(user=UserFactory(), report=r1, completed_date=t1)
    r2 = ReportSpecificationFactory()
    ReportScheduleFactory(user=user, report=r2)
    assert s1 == r1.current_schedule(user)


@pytest.mark.django_db
def test_init_report_specification():
    now = timezone.now()
    spec = ReportSpecification.objects.init_report_specification(
        slug="squares",
        title="Squares and Cubes",
        app="example_report",
        report_class="SampleReport",
    )
    assert spec.created >= now
    # run again to make check that a new schedule is not created
    now = timezone.now()
    spec = ReportSpecification.objects.init_report_specification(
        slug="squares",
        title="Squares & Cubes",
        app="example",
        report_class="XReport",
        can_update_report=False,
    )
    assert not (spec.created >= now)
    assert "Squares & Cubes" == spec.title
    assert "example" == spec.app
    assert "XReport" == spec.report_class
    assert "reports" == spec.module
    assert spec.can_update_report is False
    assert "csv" == spec.output_format


@pytest.mark.django_db
def test_init_report_specification_module():
    now = timezone.now()
    spec = ReportSpecification.objects.init_report_specification(
        slug="squares",
        title="Squares and Cubes",
        app="example_report",
        report_class="SampleReport",
        module="report",
    )
    assert spec.created >= now
    assert "Squares and Cubes" == spec.title
    assert "example_report" == spec.app
    assert "SampleReport" == spec.report_class
    assert "report" == spec.module
    assert spec.can_update_report is True
    assert "csv" == spec.output_format


@pytest.mark.django_db
def test_init_report_specification_output_format():
    now = timezone.now()
    spec = ReportSpecification.objects.init_report_specification(
        slug="squares",
        title="Squares and Cubes",
        app="example_report",
        report_class="SampleReport",
        module="report",
        output_format=ReportSpecification.FORMAT_PDF,
    )
    assert spec.created >= now
    assert "Squares and Cubes" == spec.title
    assert "example_report" == spec.app
    assert "SampleReport" == spec.report_class
    assert "report" == spec.module
    assert spec.can_update_report is True
    assert "pdf" == spec.output_format


@pytest.mark.django_db
def test_latest():
    specification = ReportSpecificationFactory()
    ReportScheduleFactory(
        report=specification, user=None, completed_date=timezone.now()
    )
    s2 = ReportScheduleFactory(
        report=specification, user=None, completed_date=timezone.now()
    )
    assert s2 == specification.latest()


@pytest.mark.django_db
def test_latest_none():
    specification = ReportSpecificationFactory()
    assert specification.latest() is None


@pytest.mark.django_db
def test_latest_user():
    user = UserFactory()
    report = ReportSpecificationFactory()
    ReportScheduleFactory(
        report=report, user=user, completed_date=timezone.now()
    )
    s2 = ReportScheduleFactory(
        report=report, user=user, completed_date=timezone.now()
    )
    ReportScheduleFactory(
        report=report, user=UserFactory(), completed_date=timezone.now()
    )
    assert s2 == report.latest(user=user)


@pytest.mark.django_db
def test_schedule():
    user = UserFactory()
    report = ReportSpecificationFactory(slug="my-report")
    report_schedule = report.schedule(user, parameters={"colour": "Green"})
    assert {"colour": "Green"} == report_schedule.parameters
    assert "" == report_schedule.title


@pytest.mark.django_db
def test_schedule_with_title():
    user = UserFactory()
    report = ReportSpecificationFactory(slug="my-report")
    report_schedule = report.schedule(user, title="My New Report")
    assert "My New Report" == report_schedule.title


@pytest.mark.django_db
def test_schedule_for_manager():
    user = UserFactory()
    report = ReportSpecificationFactory(slug="my-report")
    report_schedule = ReportSpecification.objects.schedule(
        report.slug, user, title="My New Report"
    )
    assert "My New Report" == report_schedule.title
