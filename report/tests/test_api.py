# -*- encoding: utf-8 -*-
"""Other API tests are in ``example_report/tests/test_api.py``."""
import pytest

from django.urls import reverse
from django.utils import timezone
from http import HTTPStatus

from login.tests.factories import TEST_PASSWORD, UserFactory
from report.tests.factories import ReportScheduleFactory


@pytest.mark.django_db
def test_report_schedule_api_complete(client):
    """AJAX call."""
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    report_schedule = ReportScheduleFactory(
        user=user, completed_date=timezone.now()
    )
    response = client.get(
        reverse("report.schedule.api.complete", args=[report_schedule.pk]),
        HTTP_X_REQUESTED_WITH="XMLHttpRequest",
    )
    assert HTTPStatus.OK == response.status_code
    assert {"report_completed": True} == response.json()


@pytest.mark.django_db
def test_report_schedule_api_complete_not(client):
    """AJAX call."""
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    report_schedule = ReportScheduleFactory(user=user, completed_date=None)
    response = client.get(
        reverse("report.schedule.api.complete", args=[report_schedule.pk]),
        HTTP_X_REQUESTED_WITH="XMLHttpRequest",
    )
    assert HTTPStatus.OK == response.status_code
    assert {"report_completed": False} == response.json()


@pytest.mark.django_db
def test_report_schedule_api_complete_permission_denied(client):
    """AJAX call."""
    user = UserFactory()
    assert client.login(username=user.username, password=TEST_PASSWORD) is True
    report_schedule = ReportScheduleFactory(user=UserFactory())
    response = client.get(
        reverse("report.schedule.api.complete", args=[report_schedule.pk]),
        HTTP_X_REQUESTED_WITH="XMLHttpRequest",
    )
    assert HTTPStatus.FORBIDDEN == response.status_code
