# -*- encoding: utf-8 -*-
"""Other API tests are in ``report/tests/test_api.py``."""
import pytest
import pytz

from datetime import datetime
from django.urls import reverse
from django.utils import timezone
from freezegun import freeze_time
from http import HTTPStatus
from rest_framework.exceptions import MethodNotAllowed

from api.tests.fixture import api_client_auth
from base.url_utils import url_with_querystring
from example_report.reports import SampleReport
from login.tests.factories import UserFactory
from report.models import ReportSchedule
from report.tests.factories import (
    ReportScheduleFactory,
    ReportSpecificationFactory,
)


@pytest.mark.django_db
def test_report_schedule_create(api_client_auth):
    """Create a report schedule.

    Testing the ``ReportScheduleViewSet`` class (``report/api.py``).

    """
    user = UserFactory(is_superuser=False)
    api_client = api_client_auth(user)
    report = SampleReport()
    report.init_report()
    # test
    assert 0 == ReportSchedule.objects.count()
    with freeze_time(datetime(2020, 1, 1, 1, 1, 1, tzinfo=pytz.utc)):
        data = {
            "slug": SampleReport.REPORT_SLUG,
            "comment": "Rust",
            "contacts": [],
            "description": "Programming",
            "parameters": {"contact_pk": 231, "from_date": "2021-09-12"},
        }
        url = reverse("api:report-schedule-list")
        response = api_client.post(url, data, format="json")
    # check
    assert HTTPStatus.CREATED == response.status_code, response.data
    assert 1 == ReportSchedule.objects.count()
    report_schedule = ReportSchedule.objects.first()
    assert user == report_schedule.user
    data = response.json()
    assert {
        "id": report_schedule.pk,
        "completed_date": None,
        "created": "2020-01-01T01:01:01Z",
        "download_file_name": "{}-example-sample-report-2020-01-01-01-01.csv".format(
            report_schedule.pk
        ),
        "download_url": "/report/{}/download/".format(report_schedule.pk),
        "is_complete": False,
        "parameters": {"contact_pk": 231, "from_date": "2021-09-12"},
        "retries": 0,
        "slug": None,
    } == data


@pytest.mark.django_db
def test_report_schedule_delete(api_client_auth):
    """Cannot delete a report schedule."""
    report_schedule = ReportScheduleFactory()
    api_client = api_client_auth(UserFactory())
    url = url_with_querystring(
        reverse("api:report-schedule-detail", args=[report_schedule.pk])
    )
    response = api_client.delete(url)
    assert HTTPStatus.METHOD_NOT_ALLOWED == response.status_code, response.data


@pytest.mark.django_db
def test_report_schedule_detail(api_client_auth):
    with freeze_time(datetime(2020, 1, 1, 1, 1, 1, tzinfo=pytz.utc)):
        report_schedule = ReportScheduleFactory(
            report=ReportSpecificationFactory(slug="orange"),
            completed_date=datetime(2020, 1, 31, 1, 1, 1, tzinfo=pytz.utc),
            parameters={"colour": "Orange"},
        )
    api_client = api_client_auth(UserFactory())
    url = url_with_querystring(
        reverse("api:report-schedule-detail", args=[report_schedule.pk])
    )
    response = api_client.get(url)
    assert HTTPStatus.OK == response.status_code, response.data
    data = response.json()
    assert {
        "id": report_schedule.pk,
        "completed_date": "2020-01-31T01:01:01Z",
        "created": "2020-01-01T01:01:01Z",
        "download_file_name": "{}-orange-2020-01-01-01-01.csv".format(
            report_schedule.pk
        ),
        "download_url": "/report/{}/download/".format(report_schedule.pk),
        "is_complete": True,
        "parameters": {"colour": "Orange"},
        "retries": 0,
        "slug": None,
    } == data


@pytest.mark.django_db
def test_report_schedule_update(api_client_auth):
    """Cannot update a report schedule."""
    report_schedule = ReportScheduleFactory()
    api_client = api_client_auth(UserFactory())
    url = url_with_querystring(
        reverse("api:report-schedule-detail", args=[report_schedule.pk])
    )
    response = api_client.put(url)
    assert HTTPStatus.METHOD_NOT_ALLOWED == response.status_code, response.data
