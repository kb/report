# -*- encoding: utf-8 -*-
import pytest

from reportlab.lib.enums import TA_CENTER, TA_LEFT, TA_RIGHT

from example_report.pdf import ExamplePdf
from report.pdf import PDFReport


def test_bold():
    pdf = ExamplePdf()
    para = pdf._bold("Apple")
    assert "<b>Apple</b>" == para.text
    assert TA_LEFT == para.style.alignment


def test_bold_align_centre():
    pdf = ExamplePdf()
    para = pdf._bold("Apple", align=PDFReport.CENTRE)
    assert "<b>Apple</b>" == para.text
    assert TA_CENTER == para.style.alignment


def test_bold_align_right():
    pdf = ExamplePdf()
    para = pdf._bold("Apple", align=PDFReport.RIGHT)
    assert "<b>Apple</b>" == para.text
    assert TA_RIGHT == para.style.alignment


def test_para():
    pdf = ExamplePdf()
    para = pdf._para("Orange")
    assert "Orange" == para.text
    assert TA_LEFT == para.style.alignment


def test_para_align_centre():
    pdf = ExamplePdf()
    para = pdf._para("Orange", align=PDFReport.CENTRE)
    assert "Orange" == para.text
    assert TA_CENTER == para.style.alignment


def test_para_align_right():
    pdf = ExamplePdf()
    para = pdf._para("Orange", align=PDFReport.RIGHT)
    assert "Orange" == para.text
    assert TA_RIGHT == para.style.alignment
