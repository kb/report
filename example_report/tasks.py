# -*- encoding: utf-8 -*-
import dramatiq
import logging

from django.conf import settings


logger = logging.getLogger(__name__)


@dramatiq.actor(
    queue_name=settings.DRAMATIQ_QUEUE_NAME_PIPELINE,
    max_retries=0,
    time_limit=360000,
)
def process_reports_example():
    """Process outstanding example reports.

    The default time limit is 10 minutes.
    https://dramatiq.io/guide.html#message-time-limits

    I changed this to 1 hour (I hope)!
    We have reports taking 20 minutes to run...

    """
    from report.models import ReportSchedule

    logger.info("Running example reports...")
    count = ReportSchedule.objects.process(
        queue_name=settings.DRAMATIQ_QUEUE_NAME_PIPELINE
    )
    logger.info("Run {} example reports...".format(count))
    return count
