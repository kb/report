# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [("report", "0001_initial")]

    operations = [
        migrations.AlterModelOptions(
            name="report",
            options={
                "verbose_name_plural": "Reports",
                "ordering": ("user__username", "slug"),
                "verbose_name": "Report",
            },
        )
    ]
